var data = [
        {
            "type": "symbol", "value": ["`","~"]
        },
        {
            "type": "symbol", "value": ["1","!"]
        },
        {
            "type": "symbol", "value": ["2","@"]
        },
        {
            "type": "symbol", "value": ["3","#"]
        },
        {
            "type": "symbol", "value": ["4","$"]
        },
        {
            "type": "symbol", "value": ["5","%"]
        },
        {
            "type": "symbol", "value": ["6","^"]
        },
        {
            "type": "symbol", "value": ["7","&"]
        },
        {
            "type": "symbol", "value": ["8","*"]
        },
        {
            "type": "symbol", "value": ["9","("]
        },
        {
            "type": "symbol", "value": ["0",")"]
        },
        {
            "type": "symbol", "value": ["-","_"]
        },
        {
            "type": "symbol", "value": ["=","+"]
        },
        {
            "type": "delete", "value": ["delete"], "lastitem": true
        },
        {
            "type": "tab", "value": ["tab"]
        },
        {
            "type": "letter", "value": ["q","w","e","r","t","y","u","i","o","p"]
        },
        {
            "type": "symbol", "value": ["[","{"]
        },
        {
            "type": "symbol", "value": ["]","}"]
        },
        {
            "type": "symbol", "value": ["\'","|"], "lastitem": true
        },
        {
            "type": "capslock", "value": ["caps lock"]
        },
        {
            "type": "letter", "value": ["a","s","d","f","g","h","j","k","l"]
        },
        {
            "type": "symbol", "value": [";",":"]
        },
        {
            "type": "symbol", "value": ["'",'"']
        },
        {
            "type": "return", "value": ["return"], "lastitem": true
        },
        {
            "type": "left-shift", "value": ["shift"]
        },
        {
            "type": "letter", "value": ["z","x","c","v","b","n","m",",",".","/"]
        },
        {
            "type": "right-shift", "value": ["shift"]
        },
        {
            "type": "space", "value": ["space"], "lastitem": true
        }
];
var shift = false;
if ($('.right-shift').attr('data-on') || $('.right-shift').attr('data-on')) {
    shift = true;
}

new Vue({
    el: '#app',
    data: {
        keyBoards: data,

        rightShift: false,
        leftShift: false,
        shift: false,
        capsLock: false,
        write: ''
    },
    computed: {
    },
    methods: {
        renderSpecial(type) {
            var listType = ['delete', 'tab', 'capslock', 'return', 'left-shift', 'right-shift','space'];
            if (listType.includes(type)) {
                return true;
            }

            return false;
        },
        renderClassSpecial(item) {
            var classSP = item.type;
            if (item.lastitem) {
                classSP += ' lastitem';
            }
            switch (item.type) {
                case 'capslock':
                    if (this.capsLock) {
                        classSP += ' clicked';
                    }
                    break;
                case 'right-shift':
                    if (this.rightShift) {
                        classSP += ' clicked';
                    }
                    break;
                case 'left-shift':
                    if (this.leftShift) {
                        classSP += ' clicked';
                    }
                    break;
            }

            return classSP;
        },
        clickSpecial(item)
        {
            switch (item.type) {
                case 'capslock':
                    if (this.capsLock) {
                        this.capsLock = false;
                    } else {
                        this.capsLock = true;
                    }
                    break;
                case 'right-shift':
                    this.leftShift = false;

                    if (this.rightShift) {
                        this.rightShift = false;
                    } else {
                        this.rightShift = true;
                    }
                    break;
                case 'left-shift':
                    this.rightShift = false;

                    if (this.leftShift) {
                        this.leftShift = false;
                    } else {
                        this.leftShift = true;
                    }
                    break;
                case 'symbol':
                    if (this.checkOnShift()){
                        this.write += item.value[1]
                    } else {
                        this.write += item.value[0]
                    }
                    this.resetShift();
                    break;
                case 'tab':
                    this.write += '\t';
                    break;

                case 'space':
                    this.write += ' ';
                    break;

                case 'delete':
                    this.write = this.write.slice(0, -1);
                    break;
                case 'return':
                    this.write += "\n";
            }
        },
        clickLetter(letter) {
            if (this.checkOnShift() || this.capsLock) {
                this.write += letter.toUpperCase();
                this.resetShift();
            } else {
                this.write += letter;
            }
        },
        resetShift() {
            this.leftShift = this.rightShift = false;
        },

        checkOnShift()
        {
            if (this.leftShift || this.rightShift) {
                return true;
            }
            return false;
        }
    }
});